﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movement2D : MonoBehaviour
{
    public float moveSpeed = 5f;
    public bool isGrounded = false;

    public GameObject projectileright;
    public GameObject projectileleft;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Jump();
        //
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        transform.position += movement * Time.deltaTime * moveSpeed;

        if (Input.GetKeyDown(KeyCode.P)) {
            Instantiate(projectileright, transform.position, projectileright.transform.rotation);
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            Instantiate(projectileleft, transform.position, projectileleft.transform.rotation);
        }
    }

    void Jump()
    {
        if (Input.GetButtonDown("Jump") && isGrounded == true)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 25f), ForceMode2D.Impulse);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy")) {
            SceneManager.LoadScene("Menu");
        }
    }
}
