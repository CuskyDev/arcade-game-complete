﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyoutofbounds : MonoBehaviour
{
    private float leftbound = -100;
    private float rightbound = 100;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < leftbound)
        {
            Destroy(gameObject);
        }

        if (transform.position.x > rightbound)
        {
            Destroy(gameObject);
        }

    }
}

